/**
 * C++ record manager implementation (PODC 2015) by Trevor Brown.
 * 
 * Copyright (C) 2015 Trevor Brown
 *
 */

#ifndef RECORDMGR_GLOBALS_H
#define	RECORDMGR_GLOBALS_H

#include "plaf.h"
#include "debugprinting.h"

#ifndef DEBUG
#define DEBUG if(0)
#define DEBUG2 if(0)
#endif

#ifndef MEMORY_STATS
#define MEMORY_STATS if(0)
#define MEMORY_STATS2 if(0)
#endif

// don't touch these options for crash recovery

#define CRASH_RECOVERY_USING_SETJMP
#define SEND_CRASH_RECOVERY_SIGNALS
#define AFTER_NEUTRALIZING_SET_BIT_AND_RETURN_TRUE
#define PERFORM_RESTART_IN_SIGHANDLER
#define SIGHANDLER_IDENTIFY_USING_PTHREAD_GETSPECIFIC

// some useful, data structure agnostic definitions

typedef bool CallbackReturn;
typedef void* CallbackArg;
typedef CallbackReturn (*CallbackType)(CallbackArg);

using namespace std; //for atomic types used in ds_retired_info_t 

class ds_retired_info_t {
public:
    void * obj;
    atomic_uintptr_t * ptrToObj;
    atomic_bool * nodeContainingPtrToObjIsMarked;
    ds_retired_info_t(
            void *_obj,
            atomic_uintptr_t *_ptrToObj,
            atomic_bool * _nodeContainingPtrToObjIsMarked)
            : obj(_obj),
              ptrToObj(_ptrToObj),
              nodeContainingPtrToObjIsMarked(_nodeContainingPtrToObjIsMarked) {}
    ds_retired_info_t() {}
};

inline CallbackReturn callbackCheckNotRetired(CallbackArg arg) {
    ds_retired_info_t *info = (ds_retired_info_t*) arg;
    if ((void*) info->ptrToObj->load(memory_order_relaxed) == info->obj) {
        // we insert a compiler barrier (not a memory barrier!)
        // to prevent these if statements from being merged or reordered.
        // we care because we need to see that ptrToObj == obj
        // and THEN see that ptrToObject is a field of an object
        // that is not marked. seeing both of these things,
        // in this order, implies that obj is in the data structure.
        SOFTWARE_BARRIER;
        if (!info->nodeContainingPtrToObjIsMarked->load(memory_order_relaxed)) {
            return true;
        }
    }
    return false;
}

template <typename O, typename PO, typename M, class RecManager>
bool isFailToProtectNode(const int tid, O obj, PO * ptrToObj, M* isNodeContainingPtrToObjMarked, RecManager* recordmgr, uint64_t& index/* increment after calling protect */){
    ds_retired_info_t info;
    bool didProtectionFail = false;
    const string recName = recordmgr->getReclaimerName();
    
    if (recName == "dontCare") return didProtectionFail;

    info.obj = obj;
    info.ptrToObj = (atomic_uintptr_t *) ptrToObj;
    info.nodeContainingPtrToObjIsMarked = (atomic_bool *) isNodeContainingPtrToObjMarked;
    if (recName == "hazardptr"){
        didProtectionFail = (!recordmgr->protect(tid, obj, callbackCheckNotRetired, (void*) &info));
    }
    else if(recName == "ibr" || recName == "he")
    {
        didProtectionFail = (!recordmgr->HEProtect(tid, obj, index, callbackCheckNotRetired, (void*) &info));
    }

    index++; //only needed for IBR algos
    return didProtectionFail;   
}

template <typename O, class RecManager>
void unProtectNode(const int tid, O obj, RecManager* recordmgr, uint64_t& index)
{
    index = index%(2 /*recordmgr->getMaxHzPerThread() */); //only needed for IBR algos
    const string recName = recordmgr->getReclaimerName();

    if (recName == "dontCare") return;

    if (recName == "hazardptr"){
       recordmgr->unprotect(tid, obj);
    }
    else if(recName == "ibr" || recName == "he")
    {
        recordmgr->HEUnprotect(tid, index);
    }
}

#endif	/* GLOBALS_H */
