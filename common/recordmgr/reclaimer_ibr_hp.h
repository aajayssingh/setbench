/**
 * C++ record manager implementation (PODC 2015) by Trevor Brown.
 * 
 * Copyright (C) 2015 Trevor Brown
 *
 */

#ifndef RECLAIM_IBR_HP_H
#define	RECLAIM_IBR_HP_H

#include <list>
#include "padded_primitives.h" //IBR_RCU

template <typename T = void, class Pool = pool_interface<T> >
class reclaimer_ibr_hp : public reclaimer_interface<T, Pool> {
private:
	int num_process;
	int empty_freq;
    int hp_num;
    #define IBR_HE_MAX_HP 3 //req+1 remove this hp_num should be const
	
	padded<std::atomic<T*>*>* reservations;
	padded<uint64_t>* retire_counters;
	padded<std::list<T*>>* retired; // TODO: use different structure to prevent malloc locking....

    function<bool(T** , T*, bool)> isSafe;
    PAD;

    bool can_delete(const T* const obj){
        for (int tid = 0; tid < num_process; tid++){
            for (int index = 0; index < hp_num; index++){
                if (obj != reservations[tid].ui[index].load(std::memory_order_acquire)){
                    continue;
                } else 
                {
                    //obj found in reservation list cannt delete.
                    return false;
                }
            }
        }
        return true;
    }

    void empty(const int tid){
		std::list<T*>* myTrash = &(retired[tid].ui);
		for (auto iterator = myTrash->begin(), end = myTrash->end(); iterator != end; ) {
			bool danger = false;
			auto obj = *iterator;
            if(can_delete(obj)){
				iterator = myTrash->erase(iterator);
                this->pool->add(tid, obj);
            }
            else
            {
                ++iterator;
            }
		}
		return;
    }
    
public:
    template<typename _Tp1>
    struct rebind {
        typedef reclaimer_ibr_hp<_Tp1, Pool> other;
    };
    template<typename _Tp1, typename _Tp2>
    struct rebind2 {
        typedef reclaimer_ibr_hp<_Tp1, _Tp2> other;
    };

    template <typename First, typename... Rest>
    inline bool startOp(const int tid, void * const * const reclaimers, const int numReclaimers, const bool readOnly = false) {
        bool result = true;
        return result;
    }

    inline bool isSafeFunctorAvailable(){
        return (isSafe != nullptr);
    }

    //pass &ofptr you wanna project stored in the already protected object and marked flag of already protected object.
    inline T* protect_read(const int tid, uint64_t& index, T** ptrToObj, bool& ptrToObjMarked){
        assert(isSafeFunctorAvailable());
        T* obj = *ptrToObj;
        // announce[tid]->add(obj);
        // reservations[tid].ui[index] = obj;
        reservations[tid].ui[index].store(obj, std::memory_order_seq_cst);

        // __sync_synchronize(); //if barrier is not present, atomicarraylist uses relax mem order check if their is a impiled fence or xchg and remove this aptly.
        
        if (!isSafe(ptrToObj, obj, ptrToObjMarked)){
            TRACE COUTATOMICTID ("reading ptr="<<obj<<" failed"<<std::endl);
            return NULL; // TODO return special Node;
        }
        index++;
        return obj;
    }
    inline void unprotect_read(const int tid, uint64_t& index){
        // reservations[tid].ui[index] = nullptr; //TODO: explore relaxing mem order.
        index = index%2; //num_hp //TODO:remove hardcode
        reservations[tid].ui[index].store(nullptr, std::memory_order_seq_cst);
    }


    inline bool HEProtect(const int tid, T* obj, const int index, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true)
    {
    }
    inline void HEUnprotect(const int tid, const int index){
    }


    inline void endOp(const int tid) {
		for(int index = 0; index < hp_num; index++){
			// reservations[tid].ui[index] = nullptr;
			reservations[tid].ui[index].store(nullptr, std::memory_order_seq_cst);
		}
    }
    
    // for all schemes except reference counting
    inline void retire(const int tid, T* obj) {
        if (obj == nullptr){
            return;
        }
        std::list<T*>* myTrash = &(retired[tid].ui);
        myTrash->push_back(obj);

        retire_counters[tid] = retire_counters[tid] + 1;
		if(retire_counters[tid]%empty_freq==0){
			empty(tid);
		}
    }

    void debugPrintStatus(const int tid) {
    }

    //dummy declaration
    void initThread(const int tid) { }
    void deinitThread(const int tid) { }
    inline static bool isProtected(const int tid, T * const obj) { return true;}
    inline static bool isQProtected(const int tid, T * const obj) { return false; }
    inline static bool protect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void unprotect(const int tid, T * const obj) {}
        
    inline static bool qProtect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void qUnprotectAll(const int tid) {}    
    inline static bool quiescenceIsPerRecordType() { return false;}



    /*
    * Tells whether the reclaimer requires the DS Node objects needs to provide some obj metadata to reclaimer like birthera of a node
    */
   //not needed for ibr_rcu needed for HE remove when creating HE. Here for testing of hacky birthera mechanism only.
    inline static bool needsDSObjMetaData(){
        return true;
    }
    inline static std::string getReclaimerName(){
        return "hp";
    }
    
    inline static bool supportsCrashRecovery() { return supportsPerObjectProtection(); //to avoid changing asserts in record mgr.
    }
    inline static bool supportsPerObjectProtection() { return true; }

    /**
     * To escape asserts at record manager assert(!supportcrash || isQuiescent())
    */
    inline static bool isQuiescent(const int tid) {
        return true;
    }    

    reclaimer_ibr_hp(const int numProcesses, Pool *_pool, debugInfo * const _debug, RecoveryMgr<void *> * const _recoveryMgr = NULL)
            : reclaimer_interface<T, Pool>(numProcesses, _pool, _debug, _recoveryMgr) {
        VERBOSE std::cout<<"constructor reclaimer_ibr_hp helping="<<this->shouldHelp()<<std::endl;// scanThreshold="<<scanThreshold<<std::endl;
        num_process = numProcesses;
        empty_freq = 30;
        hp_num = IBR_HE_MAX_HP;

        retired = new padded<std::list<T*>>[numProcesses];
		reservations = new padded<std::atomic<T*>*>[numProcesses]; //each reservation element is in itself a padded array of type T* with size hp_num. 

		for (int i = 0; i<numProcesses; i++){
            reservations[i].ui = new  std::atomic<T*>[hp_num];
            for (int j = 0; j < hp_num; j++){
    			reservations[i].ui[j].store(nullptr,std::memory_order_release); //TODO: explore relaxing mem order. 
            }
		}
		retire_counters = new padded<uint64_t>[numProcesses];
        isSafe = [](T** ptrToObj, T* obj, bool nodeContainingPtrToObjIsMarked){ if( (*ptrToObj) == obj){ if(!nodeContainingPtrToObjIsMarked){ TRACE COUTATOMICTID("in functor="<<obj<<std::endl); return true;} } return false; };

    }
    ~reclaimer_ibr_hp() {
        // TODO: Shouldn't I clean up?
    }

};

#endif //RECLAIM_IBR_HP_H
