/**
 * C++ record manager implementation (PODC 2015) by Trevor Brown.
 * 
 * Copyright (C) 2015 Trevor Brown
 *
 */

#ifndef RECLAIM_IBR_RCU_H
#define	RECLAIM_IBR_RCU_H

#include <list>
#include "padded_primitives.h" //IBR_RCU
// #include "allocator_interface.h"
// #include "reclaimer_interface.h"



template <typename T = void, class Pool = pool_interface<T> >
class reclaimer_ibr_rcu : public reclaimer_interface<T, Pool> {
private:
	int num_process;
	int empty_freq;
	int epoch_freq;
	
public:
    class RCUInfo{
	public:
		T* obj;
		uint64_t epoch;
		RCUInfo(T* obj, uint64_t epoch):obj(obj),epoch(epoch){}
	};
	
private:
	paddedAtomic<uint64_t>* reservations;
	padded<std::list<RCUInfo>>* retired; 
	padded<uint64_t>* retire_counters;
	padded<uint64_t>* alloc_counters;

	std::atomic<uint64_t> epoch;
    PAD;

    inline uint64_t getEpoch() { 
		return epoch.load(std::memory_order_acquire);
    }

    void empty(const int tid){
        uint64_t minEpoch = UINT64_MAX;
        for (int i = 0; i<num_process; i++){
            uint64_t res = reservations[i].ui.load(std::memory_order_acquire);
            if(res<minEpoch){
                minEpoch = res;
            }
        }
        // erase safe objects
        std::list<RCUInfo>* myTrash = &(retired[tid].ui);
        for (auto iterator = myTrash->begin(), end = myTrash->end(); iterator != end; ) {
            RCUInfo res = *iterator;
            if(res.epoch<minEpoch){
                iterator = myTrash->erase(iterator);//return iterator corresponding to next of last erased item
                this->pool->add(tid, res.obj);
                // this->reclaim(res.obj);
                // this->dec_retired(tid);
            }
            else{++iterator;}
        }
    }
    
public:
    template<typename _Tp1>
    struct rebind {
        typedef reclaimer_ibr_rcu<_Tp1, Pool> other;
    };
    template<typename _Tp1, typename _Tp2>
    struct rebind2 {
        typedef reclaimer_ibr_rcu<_Tp1, _Tp2> other;
    };
    
    inline static bool quiescenceIsPerRecordType() { return false; }
    
    template <typename First, typename... Rest>
    inline bool startOp(const int tid, void * const * const reclaimers, const int numReclaimers, const bool readOnly = false) {
        bool result = true;

        uint64_t e = epoch.load(std::memory_order_acquire);
        reservations[tid].ui.store(e,std::memory_order_seq_cst);
		alloc_counters[tid]=alloc_counters[tid]+1;

        return result;
    }
    
    inline void endOp(const int tid) {
        reservations[tid].ui.store(UINT64_MAX,std::memory_order_seq_cst);
    }
    
    // for all schemes except reference counting
    inline void retire(const int tid, T* obj) {
		if(obj==NULL){return;}
		std::list<RCUInfo>* myTrash = &(retired[tid].ui);
		// for(auto it = myTrash->begin(); it!=myTrash->end(); it++){
		// 	assert(it->obj!=obj && "double retire error");
		// }
			
		uint64_t e = epoch.load(std::memory_order_acquire);
		RCUInfo info = RCUInfo(obj,e);
		myTrash->push_back(info);
		retire_counters[tid]=retire_counters[tid]+1;
        if(alloc_counters[tid]%(epoch_freq*num_process)==0){
			epoch.fetch_add(1,std::memory_order_acq_rel);
		}

		if(retire_counters[tid]%empty_freq==0){
			empty(tid);
		}        
    }

    //pass &ofptr you wanna project stored in the already protected object and marked flag of already protected object.
    inline T* protect_read(const int tid, uint64_t& index, T** ptrToObj, bool& ptrToObjMarked){
        T* obj = *ptrToObj;
        return obj;
    }
    inline void unprotect_read(const int tid, uint64_t& index){
    }
    
    void debugPrintStatus(const int tid) {
        if (tid == 0) {
            std::cout<<"global_epoch_counter="<<epoch.load(std::memory_order_acquire)<<std::endl;
        }
    }

    //dummy declaration
    void initThread(const int tid) { }
    void deinitThread(const int tid) { }
    inline static bool isProtected(const int tid, T * const obj) { return true;}
    inline static bool isQProtected(const int tid, T * const obj) { return false; }
    inline static bool protect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void unprotect(const int tid, T * const obj) {}
    inline static bool qProtect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void qUnprotectAll(const int tid) {}    

    reclaimer_ibr_rcu(const int numProcesses, Pool *_pool, debugInfo * const _debug, RecoveryMgr<void *> * const _recoveryMgr = NULL)
            : reclaimer_interface<T, Pool>(numProcesses, _pool, _debug, _recoveryMgr) {
        VERBOSE std::cout<<"constructor reclaimer_ibr_rcu helping="<<this->shouldHelp()<<std::endl;// scanThreshold="<<scanThreshold<<std::endl;
        num_process = numProcesses;
        empty_freq = 30; //this was default values ion IBR microbench
        epoch_freq = 150; //this was default values ion IBR microbench

        retired = new padded<std::list<RCUInfo>>[numProcesses];
		reservations = new paddedAtomic<uint64_t>[numProcesses];
		retire_counters = new padded<uint64_t>[numProcesses];
		alloc_counters = new padded<uint64_t>[numProcesses];

		for (int i = 0; i<numProcesses; i++){
			reservations[i].ui.store(UINT64_MAX,std::memory_order_release);
			retired[i].ui.clear();
		}
		epoch.store(0,std::memory_order_release);
    }
    ~reclaimer_ibr_rcu() {
    }

};

#endif //RECLAIM_IBR_RCU_H


