/*
 * TODO:
 * remove unnecessary headers
 * Memory Layout of variables
 */
/*
 * CONSTRAINTS:
 * File name has to match class name
 */

#ifndef RECLAIMER_TR_H
#define RECLAIMER_TR_H

#include <cassert>
#include <iostream>
#include "plaf.h"
#include "globals.h"
#include "blockbag.h"
#include "allocator_interface.h"
#include "reclaimer_interface.h"
#include "arraylist.h"
#include "hashtable.h"
#include "record_manager_single_type.h"

template <typename T = void, class Pool = pool_interface<T> >
class reclaimer_tr : public reclaimer_interface<T, Pool>{
    private:
        //all macros here
        #define MAX_PER_THREAD_HAZARDPTR 3//2.  //3 for guerraoui delete, and 2 for herl;ihy's list.
        #define OPS_BEFORE_NEUTRALIZE 1000
        unsigned int num_process;
        PAD;
        //all internal data structures used
        
        //all bags get created per record type. That is retiredBag for Node and descp records will exist. Similarly proposedHzp
        //will get created per record type. Ensure that record manager calls the ops updating them for each record type 
        //or the functions get called only once  but the operation for each record type is handled within the reclaimer's operation itself.
//        blockbag<T> **spareBag;
        blockbag<T> **retiredBag;                                   // retiredBag[tid] = retired records collected by deleteOP
        PAD;
        AtomicArrayList<T> **proposedHzptrs;              // proposedHzptrs[tid] = saves the discovered records  before upgrading to write to protect records from concurrent reclaimer threads.
        PAD;
        hashset_new<T> **scannedHzptrs;                     // scannedHzptrs[tid] =  each reclaimer tid scans HzPtrs across threads to free retired its bag such that anyHzptr aren't freed.
        PAD;
        //all internal variables
        const unsigned int retiredBagSizePatience = 3;//25;//50; //Tree performs best for 3 and 5. higher value (25, 100)leads to less recycling of nodes 
                                                        //thus causes per drop with threads. Low value like 1 causes more signaling overhead
        //static inline thread_local bool restartableCopy = 0;      // copy of restartable variable currently declared in recovery.h to be used in future.
        sigset_t neutralizeSignalSet;
        PAD;
        
        inline bool isOutOfPatience(const int tid){
            return (retiredBag[tid]->getSizeInBlocks() > (retiredBagSizePatience * num_process));
        }
        
        bool requestAllThreadsToRestart(const int tid){
            //pthread_kill to all threads except self
            bool result = false;
            for(int otherTid = 0; otherTid < this->NUM_PROCESSES; ++otherTid){
                //send signal if not self
                if (tid != otherTid){
                    pthread_t otherPthread = this->recoveryMgr->getPthread(otherTid);
                    int error = 0;
                    if( error = pthread_kill (otherPthread, this->recoveryMgr->neutralizeSignal) ){
                        COUTATOMICTID("Error when trying to pthread_kill(pthread_tFor("<<otherTid<<"), "<<this->recoveryMgr->neutralizeSignal<<")"<<std::endl);
                        if(error == ESRCH) COUTATOMICTID("ESRCH"<<std::endl);
                        if (error == EINVAL)  COUTATOMICTID("EINVAL"<<std::endl);
                        TRACE assert("Error when trying to pthread_kill" && 0);
                        return result;
                    }
                    else{
                        TRACE COUTATOMICTID(" Signal sent via pthread_kill(pthread_tFor("<<otherTid<<"  " <<otherPthread <<"), "<<this->recoveryMgr->neutralizeSignal<<")"<<std::endl);
                    }                    
                }// if (tid != otherTid){                
            }// for()
            result = true;
            return result;
        }
        
        inline void collectAllSavedRecords(const int tid){
            scannedHzptrs[tid]->clear(); //set where record would be collected in.
            TRACE assert("scannedHzptrs[tid] size should be 0 before collection" && scannedHzptrs[tid]->size() == 0);
            
            //OPT: should skip collecting own Hzptrs
            for (int otherTid = 0; otherTid <  this->NUM_PROCESSES; ++otherTid)
            {
                unsigned int sz = proposedHzptrs[otherTid]->size(); //size shouldn't change during execution.
                TRACE assert ("prposedHzptr[othertid] should be less than max" && sz <= MAX_PER_THREAD_HAZARDPTR);
                for (int ixHP = 0; ixHP < sz/*Can do this as sz is const*/; ++ixHP){
                    T * hp = (T*) proposedHzptrs[otherTid]->get(ixHP);//acquired_hazardptrs size never changes.
                    if (hp){
                        scannedHzptrs[tid]->insert((T*)hp);
                    }
                }
            }
        }
        
        inline void sendFreeableRecordsToPool(const int tid){
            blockbag<T> * const freeable = retiredBag[tid];

            GSTATS_APPEND(tid, limbo_reclamation_event_size, freeable->computeSize());

            unsigned int assert_sz_check = freeable->computeSize();//REMOVEME
            TRACE COUTATOMICTID("TR:: sigSafe: sendFreeableRecordsToPool: Before freeing retiredBag size in nodes="<<freeable->computeSize()<<std::endl);
//            assert ("TEMP assert: not all of the bag should be Hzptr protected" && scannedHzptrs[tid]->size() < freeable->computeSize()); //REMOVEME
            //Now collected all hazard pointer protected records. Send my retired records to pool except the Hp protected ones.
#if 0 //forlist
            blockbag_iterator<T> it =  freeable->begin();
            while(it != freeable->end()){
                if (scannedHzptrs[tid]->contains(*it))
                    assert ("assumption that Hzbag intersects with free bag failed" && 0);
                it++;
            }
            this->pool->addMoveFullBlocks(tid, freeable);
            
#elif 0 //for list and tree         
            //Now collected all hazard pointer protected records. Send my retired records to pool except the Hp protected ones.
            blockbag_iterator<T> it =  freeable->begin();
            blockbag_iterator<T> nextswap =  freeable->begin();
//            int itc=0, currc=0;
            while(it != freeable->end())
            {
                if (scannedHzptrs[tid]->contains(*it))
                {
                    it.swap(nextswap.getCurr(), nextswap.getIndex());
                    nextswap++;
//                    currc++;//
                }
                it++;
//                itc++;//
            }
            block<T>* const curr = nextswap.getCurr();
            if (curr){
//                COUTATOMICTID("tr:addmovefullblock\n"<<itc<<" : "<<currc<<std::endl);
                this->pool->addMoveFullBlocks(tid, freeable, curr); //this leaks memory.
            }
#else 
       
            //Now collected all hazard pointer protected records. Send my retired records to pool except the Hp protected ones.
            blockbag<T> * const sparebag = new blockbag<T>(tid, this->pool->blockpools[tid]);
            blockbag_iterator<T> it =  freeable->begin();
            int itc=0, currc=0;
            
            T* ptr;
            while (1) {                
                if (freeable->isEmpty()) break;
                
                ptr = freeable->remove();
                
//                COUTATOMICTID(ptr->key<<std::endl);
                if (scannedHzptrs[tid]->contains(ptr))
                {
                    sparebag->add(ptr);
                    TRACE COUTATOMICTID(ptr->key<<std::endl);
                }
                else
                {
                    this->pool->add(tid, ptr);
                }
            }
            
            while (1)
            {
                if (sparebag->isEmpty()) break;
                ptr = sparebag->remove();
                freeable->add(ptr);
            }
#endif
            TRACE COUTATOMICTID("TR:: sigSafe: sendFreeableRecordsToPool: After freeing retiredBag size in nodes="<<retiredBag[tid]->computeSize()<<" freeable="<<freeable->computeSize()<<std::endl);
//            assert ("check retiredbag size reduced ..."&& retiredBag[tid]->computeSize() < assert_sz_check );
        }
        
        //NOTICEME: Currently not freeing descriptors only Nodes record type is freed.
        inline bool reclaimFreeable(const int tid){
            bool result = false;
            //collectSavedRecords() in scannedHzptrs[tid] ;
            collectAllSavedRecords(tid);
            
            //send retired records to Pool to free
            sendFreeableRecordsToPool(tid);            
            return true;
        }
    
    public:
        reclaimer_tr(const int numProcess, Pool *_pool, debugInfo *const  _debug, RecoveryMgr<void*> * const _recoveryMgr = NULL) : reclaimer_interface<T, Pool> (numProcess, _pool, _debug, _recoveryMgr) {
            
            COUTATOMIC("constructor reclaimer_tr helping="<<this->shouldHelp()<<std::endl);// NOTICEME: Not sure why help me should be used here copying d+;
            num_process = numProcess;
            if (_recoveryMgr) COUTATOMIC("SIGRTMIN="<<SIGRTMIN<<" neutralizeSignal="<<this->recoveryMgr->neutralizeSignal<<std::endl);
            
            // set up signal set for neutralize signal
            //NOTICEME: Not sure why doing this as it isnt being used in whole of setbench, but copying debra plus as of now.
            if (sigemptyset(&neutralizeSignalSet)) {
                COUTATOMIC("error creating empty signal set"<<std::endl);
                exit(-1);
            }
            if (_recoveryMgr) {
                if (sigaddset(&neutralizeSignalSet, this->recoveryMgr->neutralizeSignal)) {
                    COUTATOMIC("error adding signal to signal set"<<std::endl);
                    exit(-1);
                 }
            }
            
            //init all reclaimer ds
            retiredBag = new blockbag<T> * [numProcess*PREFETCH_SIZE_WORDS];
            proposedHzptrs = new AtomicArrayList<T> * [numProcess*PREFETCH_SIZE_WORDS];
            scannedHzptrs = new hashset_new<T> * [numProcess*PREFETCH_SIZE_WORDS];
            
            for (int tid  = 0; tid < numProcess; ++tid){
                retiredBag[tid] = new blockbag<T>(tid, this->pool->blockpools[tid]);
                proposedHzptrs[tid] = new AtomicArrayList<T>(MAX_PER_THREAD_HAZARDPTR);
                scannedHzptrs[tid] = new hashset_new<T>(numProcess*MAX_PER_THREAD_HAZARDPTR);
            }
            //TODO: assert initial state of each DS
            
            //init simple variables
        }
                
        ~reclaimer_tr(){
           VERBOSE DEBUG COUTATOMIC("destructor reclaimer_tr"<<std::endl);
           //temp stat vars to calculate avgs for signal handler and #longjmps
           long long avgsigexec = 0;
           long long avgrestarts = 0;
           
            for (int tid  = 0; tid < this->NUM_PROCESSES; ++tid){ //NUM_PROCESSES from reclaimer_interface
                //move any remaining records to free.Else dtor of blockbag asserts.
                this->pool->addMoveAll(tid, retiredBag[tid]);
                //call DS dtors
                delete retiredBag[tid];
                delete proposedHzptrs[tid];
                delete scannedHzptrs[tid];
                
                avgsigexec += countInterrupted.get(tid);
                avgrestarts += countLongjmp.get(tid);
            }
            //delete array 
            delete[] retiredBag;
            delete[] proposedHzptrs;
            delete[] scannedHzptrs;
            COUTATOMIC("avgsigexec="<<avgsigexec/num_process<<std::endl);
            COUTATOMIC("avglongjmprestarts="<<avgrestarts/num_process<<std::endl);
        }
        
        inline void clearHzBags(const int tid){
            COUTATOMICTID("TR:: clearHzBags: NotSigSafe"<<" typeid(T)="<<typeid(T).name()<<"HzBagSize="<<proposedHzptrs[tid]->size()<<std::endl);
            proposedHzptrs[tid]->clear();
            TRACE assert ("proposedHzptrs[tid]->size should be 0" && proposedHzptrs[tid]->size() == 0);
        }
        
        /*
         * USERWARNING: startOp should always be called after setjmp. Since, if setjmp done later than startOP the restartable would never be set to 1 and Upgrade assert willfail.
         */
        template <typename First, typename... Rest>
        inline bool startOp(const int tid, void * const * const reclaimers, const int numReclaimers, const bool readOnly = false) {
            bool result = false;
            
            TRACE COUTATOMICTID("TR:: startOp: NotSigSafe"<<" typeid(T)="<<typeid(T).name()<<std::endl);
            TRACE assert ("restartable value should be 0 in before startOp"&& restartable == 0);
            //after restart I may have Hzptrs saved from last iteration. Either clear in Handler or clear here.
            //Clearing in handler would render it not general. Right place could be the alternate path in setjmp loop.
            //HZBAG CLEARINGPHASE 
//            for (int i = 0; i < numReclaimers; ++i){
//                ( ( reclaimer_tr<T, Pool> * const ) reclaimers[i] )->clearHzBags(tid);
//            }            
            proposedHzptrs[tid]->clear(); //works only for single type
            TRACE assert ("proposedHzptrs[tid]->size should be 0" && proposedHzptrs[tid]->size() == 0);
            //HZBAG CLEARINGPHASE
            
            TRACE assert ("restartable should be 0. Else you have violated semantic of OP or b4sigjmp not reseted restartable" && restartable == 0);
//            restartable = 1; //discovery begin
#ifdef RELAXED_RESTARTABLE
            restartable = 1;
#else
            CASB (&restartable, 0, 1);//assert(CASB (&restartable, 0, 1));
#endif
            
            TRACE assert ("restartable value should be 1"&& restartable == 1);
            result = true; //can this be reordered before its init?
            return result;
        }
        
        /*
         *  to prevent any records you may want other threads to not reclaim.
         */
        inline void saveForWritePhase(const int tid, T * const record){
            TRACE COUTATOMICTID("TR:: saveForWritePhase: NotSigSafe"<<" typeid(T)="<<typeid(T).name()<<std::endl);
            TRACE assert ("proposedHzptrs ds should be non-null" && proposedHzptrs);
            TRACE assert ("record to be added should be non-null" && record);
            if (record == NULL)  assert ("null record"&& 0);
//            COUTATOMICTID("***********************b4 HzBag sz="<<proposedHzptrs[tid]->size()<<std::endl);
            TRACE assert ("HzBag IS full" && !proposedHzptrs[tid]->isFull());
            proposedHzptrs[tid]->add(record);
//            COUTATOMICTID("***********************a4 HzBag sz="<<proposedHzptrs[tid]->size()<<std::endl);
        }
        
        /*
         * 
         */
        inline void upgradeToWritePhase(const int tid){
            TRACE COUTATOMICTID("TR:: upgradeToWritePhase: SigSafe"<<std::endl);
           //assert("CAS shoudn't fail ever" && CASB (&restartable, 1, 0));
            TRACE assert ("restartable value should be 1 before write phase"&& restartable == 1);
#ifdef RELAXED_RESTARTABLE
            restartable = 0;
#else
            CASB (&restartable, 1, 0);//assert (CASB (&restartable, 1, 0));
#endif
            TRACE assert ("restartable value should be 0 in write phase"&& restartable == 0);
        }
        
        /*
         * Use the API to clear any operation specific ds which won't be required across operations.
         *  i) clear ds populated in saveForWrite
         *  ii) USERWARNING: any record saved for write phase must be released in this API by user. Thus user should call this API at all control flows that return from dsOP.
         */
        inline void endOp(const int tid){
            TRACE COUTATOMICTID("TR:: endOp: NotSigSafe"<<" typeid(T)="<<typeid(T).name()<<std::endl);
            TRACE assert ("proposedHzptrs ds should be non-null" && proposedHzptrs);
            proposedHzptrs[tid]->clear(); //@J since this is a simple store 0 even if interrupted or siglongjmped no issues.
            TRACE assert ("proposedHzptrs[tid] should have size 0" && proposedHzptrs[tid]->size() == 0);
#ifdef RELAXED_RESTARTABLE
            restartable = 0;
#else
            CASB (&restartable, restartable, 0);
#endif
            //assert ("CAS endOP" && CASB (&restartable, restartable, 0));
            TRACE assert ("restartable value should be 0 in post endOP"&& restartable == 0);
        }
        
        /*
         * Tells whether the reclaimer supports signalling
         */
        inline static bool needsSetJmp(){
            return true;
        }
        
        /*
         *  retire not only gets called by deletes. But also by helps in ins or lookup. DS may populate Hz bags from helps thus exceed the size limit. Need to protect descpritors as well.
         */
        inline void retire(const int tid, T* record){
            TRACE COUTATOMICTID("TR:: retire: NotSigSafe"<<" typeid(T)="<<typeid(T).name()<<std::endl);
            
                       //RECLAIMATION PHASE
            //OPT: getblocksize is faster than computesize in records
            if (isOutOfPatience(tid)){
                TRACE COUTATOMICTID("TR:: outOfPatience: SigSafe: retiredBag BlockSize="<< retiredBag[tid]->getSizeInBlocks()<<"retiredBag Size nodes="<<retiredBag[tid]->computeSize()<<std::endl);
#ifdef NORECJMP_ONLYSIG // To just check signals without siglongjmps and without freeing records
               requestAllThreadsToRestart(tid);
#else                
                if (requestAllThreadsToRestart(tid)){
                    TRACE COUTATOMICTID("TR:: sigSafe: outOfPatience: restarted all threads, gonna continue reclaim ="<<retiredBag[tid]->getSizeInBlocks()<<" block"<<std::endl);
                    //NOTICEME: only frees one record type. Needs to free all records type.
                    reclaimFreeable(tid);
                }
                else{
                    COUTATOMICTID("TR:: outOfPatience: Couldn't restart all threads, gonna continue ops"<<std::endl);
                    TRACE assert ("Should have restarted all threads..." && 0);
                    exit (-1);
                }
#endif
            } // if (isOutOfPatience(tid)){
           //RECLAIMATION PHASE
            
            TRACE assert ("retiredBag ds should be non-null" && retiredBag);
            TRACE assert ("record to be added should be non-null" && record);
            if (record == NULL) assert ("null record"&& 0);
            retiredBag[tid]->add(record);                
        }
        
        void debugPrintStatus(const int tid){
            TRACE COUTATOMICTID ("retiredBag Size in blocks:"<<retiredBag[tid]->getSizeInBlocks()<<std::endl);
            TRACE COUTATOMICTID ("retiredBag Size:"<<retiredBag[tid]->computeSize() <<std::endl);
            
            TRACE COUTATOMICTID("#handlerexec="<<countInterrupted.get(tid)<<std::endl);
            TRACE COUTATOMICTID("#longjmps="<<countLongjmp.get(tid)<<std::endl<<std::endl);
        }
        
        //template aliasing
        template<typename _Tp1>
        struct rebind {
            typedef reclaimer_tr<_Tp1, Pool> other;
        };
        template<typename _Tp1, typename _Tp2>
        struct rebind2{
            typedef reclaimer_tr<_Tp1, _Tp2> other;
        };
        
        //dummy definitions to prevent error/warning of legacy code
        inline bool protect(const int tid, T* obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true){return false;}
        inline bool qProtect(const int tid, T* obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true){return false;}
        inline void enterQuiescentState(const int tid){}
        inline bool leaveQuiescentState(const int tid, void * const * const reclaimers, const int numReclaimers){ return false;}
        /*
         * Important to define these and not leave them empty. So that registered tid for the recovery.h gets inited. Currently in the setbench code
         *  initTHread and deinit thread does n't get called.
         */
        void initThread(const int tid) {
//            AJDBG COUTATOMICTID("tr::pthreadself:"<<pthread_self()<<" registeredtid:"<<registeredThreads[tid]<<std::endl); //@J            
        }
        void deinitThread(const int tid) {
//            AJDBG COUTATOMICTID("tr::pthreadself:"<<pthread_self()<<" registeredtid:"<<registeredThreads[tid]<<std::endl); //@J
        }
        //NOTICEME:  if I dont write false, them startOp will be called for each rectype and which will cause startOp assrt to fail. As startOp called twice w/o matching endOP 
        //to reset restaratable to 0.
        inline static bool quiescenceIsPerRecordType() { return false; }
        
        
};

#endif /* RECLAIMER_TR_H */

