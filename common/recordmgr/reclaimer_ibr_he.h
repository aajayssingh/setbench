/**
 * C++ record manager implementation (PODC 2015) by Trevor Brown.
 * 
 * Copyright (C) 2015 Trevor Brown
 *
 */

#ifndef RECLAIM_IBR_HE_H
#define	RECLAIM_IBR_HE_H

#include <list>
#include "padded_primitives.h" //IBR_RCU
// #include "allocator_interface.h"
// #include "reclaimer_interface.h"

template <typename T = void, class Pool = pool_interface<T> >
class reclaimer_ibr_he : public reclaimer_interface<T, Pool> {
private:
	int num_process;
	int empty_freq;
	int epoch_freq;
    int he_num;
    #define IBR_HE_MAX_HE 2

    function<bool(T** , T*, bool)> isSafe;
    PAD;

public:
    class HEInfo{
	public:
		T* obj;
		uint64_t birth_epoch;
		uint64_t retire_epoch;
		HEInfo(T* obj, uint64_t b_epoch, uint64_t r_epoch):obj(obj), birth_epoch(b_epoch), retire_epoch(r_epoch){}
	};
    T* NOT_SAFE;
	
private:
	padded<std::atomic<uint64_t>*>* reservations; //Each thread reserves a fixed number of epochs like HP reserves pointers.
	padded<std::list<HEInfo>>* retired; 
	padded<uint64_t>* retire_counters;
	padded<uint64_t>* alloc_counters;

	std::atomic<uint64_t> epoch;
    PAD;

    inline uint64_t getEpoch() { 
		return epoch.load(std::memory_order_seq_cst);
    }

    bool can_delete(uint64_t birth_epoch, uint64_t retire_epoch){
        for (int i = 0; i < num_process; i++){
            for (int j = 0; j < he_num; j++){
                const uint64_t epo = reservations[i].ui[j].load(std::memory_order_seq_cst);
                if (epo < birth_epoch || epo > retire_epoch || epo == 0){
                    continue;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    void empty(const int tid){
		// erase safe objects
		std::list<HEInfo>* myTrash = &(retired[tid].ui);
		for (auto iterator = myTrash->begin(), end = myTrash->end(); iterator != end; ) {
			HEInfo res = *iterator;
			if(can_delete(res.birth_epoch, res.retire_epoch)){
				// reclaim(res.obj);
				iterator = myTrash->erase(iterator);
                this->pool->add(tid, res.obj);
				// this->dec_retired(tid);
			}
			else{++iterator;}
		}
    }
    
public:
    template<typename _Tp1>
    struct rebind {
        typedef reclaimer_ibr_he<_Tp1, Pool> other;
    };
    template<typename _Tp1, typename _Tp2>
    struct rebind2 {
        typedef reclaimer_ibr_he<_Tp1, _Tp2> other;
    };

    template <typename First, typename... Rest>
    inline bool startOp(const int tid, void * const * const reclaimers, const int numReclaimers, const bool readOnly = false) {
        bool result = true;
        alloc_counters[tid]=alloc_counters[tid]+1; //make shift arrangement to let epoch advance. NOt the place where orig HE does this.
        if(alloc_counters[tid]%(epoch_freq*num_process)==0){ //this not the pace where original HE inc epoch
			epoch.fetch_add(1,std::memory_order_seq_cst);
		}

        
        return result;
    }

    /**
     * Inner utility method for protect
     * DEBUG Hints: Could be that I am messing with order of following ops load prevEpoch, read obj, load currEpoch. 
     *
     */
    void read(T* obj, const uint64_t& index, const int tid){
        uint64_t prev_epoch = reservations[tid].ui[index].load(std::memory_order_seq_cst);
        while(true){
            uint64_t curr_epoch = getEpoch();
            if (curr_epoch == prev_epoch){ //just a fast path to avoid a costly store
                assert (curr_epoch == prev_epoch);
                return;
            } 
            else {
                reservations[tid].ui[index].store(curr_epoch, std::memory_order_seq_cst);
                prev_epoch = curr_epoch;
            }
        }
    }

    inline bool isSafeFunctorAvailable(){
        return (isSafe != nullptr);
    }

    //pass &ofptr you wanna project stored in the already protected object and marked flag of already protected object.
    inline T* protect_read(const int tid, uint64_t& index, T** ptrToObj, bool& ptrToObjMarked){
        assert(isSafeFunctorAvailable());
        T* obj = *ptrToObj;
        read(obj, index, tid); //reserves the current epoch.

//        __sync_synchronize();

        if (!isSafe(ptrToObj, obj, ptrToObjMarked)){
            TRACE COUTATOMICTID ("reading ptr="<<obj<<" failed"<<std::endl);
            return NOT_SAFE; // TODO return special Node;
        }
        index++;
        return obj;
    }
    inline void unprotect_read(const int tid, uint64_t& index){
        index = index%he_num; //num_hp //TODO:remove hardcode
        reservations[tid].ui[index].store(0, std::memory_order_seq_cst);
    }

    inline void endOp(const int tid) {
        // for (int i = 0; i < he_num; i++){
        //     reservations[tid].ui[i].store(0, std::memory_order_seq_cst);
        // }
    }
    
    inline void retire(const int tid, T* obj) {
		if(obj==NULL){return;}
        uint64_t birth_epoch = read_birth(obj);

		std::list<HEInfo>* myTrash = &(retired[tid].ui);
		for(auto it = myTrash->begin(); it!=myTrash->end(); it++){
			assert(it->obj!=obj && "double retire error");
		}
			
		uint64_t retire_epoch = epoch.load(std::memory_order_seq_cst);
		myTrash->push_back(HEInfo(obj, birth_epoch, retire_epoch));
		
        retire_counters[tid] = retire_counters[tid] + 1;
		if(retire_counters[tid]%empty_freq==0){
			empty(tid);
		}
    }

    void debugPrintStatus(const int tid) {
        if (tid == 0) {
            std::cout<<"global_epoch_counter="<<epoch.load(std::memory_order_seq_cst)<<std::endl;
        }
    }

    /*
    * Tells whether the reclaimer requires the DS Node objects needs to provide some obj metadata to reclaimer like birthera of a node
    */
   //not needed for ibr_rcu needed for HE remove when creating HE. Here for testing of hacky birthera mechanism only.
    inline static bool needsDSObjMetaData(){
        return true;
    }
    inline static std::string getReclaimerName(){
        return "he";
    }
    inline T* getNotSafeNode(){
        return NOT_SAFE;
    }
    
    inline static bool supportsCrashRecovery() { return supportsPerObjectProtection(); //to avoid changing asserts in record mgr.
    }
    inline static bool supportsPerObjectProtection() { return true; }

    /* to read birth epoch from object. Shall be private. */
	uint64_t read_birth(T* obj){
        return obj->birth_epoch;
    }

#ifdef BIRTH_EPOCH_NEEDED
    //this is public API which shall call privateAPI. 
    inline uint64_t getBirthEpoch(){
        return getEpoch();
    }
#endif // BIRTH_EPOCH_NEEDED


    //dummy declaration
    void initThread(const int tid) { }
    void deinitThread(const int tid) { }
    inline static bool isProtected(const int tid, T * const obj) { return true;}
    inline static bool isQProtected(const int tid, T * const obj) { return false; }
    inline static bool protect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void unprotect(const int tid, T * const obj) {}
        
    inline static bool qProtect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void qUnprotectAll(const int tid) {}    
    inline static bool quiescenceIsPerRecordType() { return false;}

    /**
     * To escape asserts at record manager assert(!supportcrash || isQuiescent())
    */
    inline static bool isQuiescent(const int tid) {
        return true;
    }    

    reclaimer_ibr_he(const int numProcesses, Pool *_pool, debugInfo * const _debug, RecoveryMgr<void *> * const _recoveryMgr = NULL)
            : reclaimer_interface<T, Pool>(numProcesses, _pool, _debug, _recoveryMgr) {
        VERBOSE std::cout<<"constructor reclaimer_ibr_he helping="<<this->shouldHelp()<<std::endl;// scanThreshold="<<scanThreshold<<std::endl;
        num_process = numProcesses;
        empty_freq = 30;
        epoch_freq = 150;
        he_num = IBR_HE_MAX_HE;

        retired = new padded<std::list<HEInfo>>[numProcesses];
		reservations = new padded<std::atomic<uint64_t>*>[numProcesses];

		for (int i = 0; i<numProcesses; i++){
            reservations[i].ui = new  std::atomic<uint64_t>[he_num];
            for (int j = 0; j < he_num; j++){
    			reservations[i].ui[j].store(0,std::memory_order_release);
            }
			retired[i].ui.clear(); //clear list
		}
		retire_counters = new padded<uint64_t>[numProcesses];
		alloc_counters = new padded<uint64_t>[numProcesses];
		epoch.store(0,std::memory_order_release);

        NOT_SAFE = new T();
        isSafe = [](T** ptrToObj, T* obj, bool nodeContainingPtrToObjIsMarked){ if( (*ptrToObj) == obj){ if(!nodeContainingPtrToObjIsMarked){ TRACE COUTATOMICTID("in functor="<<obj<<std::endl); return true;} } return false; };
    }
    ~reclaimer_ibr_he() {
        delete NOT_SAFE;
    }

};

#endif //RECLAIM_IBR_HE_H


