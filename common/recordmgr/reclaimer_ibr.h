/**
 * Adapted from IBR's 2GEIBR algorithm. 
 * source: https://github.com/roghnin/Interval-Based-Reclamation, By Haosen Wen, Joseph Izraelevitz, Wentao Cai, H. Alan Beadle, Michael L. Scott
 */

#ifndef RECLAIM_IBR_H
#define	RECLAIM_IBR_H

#include <list>
#include "padded_primitives.h"

template <typename T = void, class Pool = pool_interface<T> >
class reclaimer_ibr : public reclaimer_interface<T, Pool> {
private:
    int task_num;
    int freq;
    int epochFreq;
public:
    class IntervalInfo{
    public:
        T* obj;
        uint64_t birth_epoch;
        uint64_t retire_epoch;
        IntervalInfo(T* obj, uint64_t b_epoch, uint64_t r_epoch): obj(obj), birth_epoch(b_epoch), retire_epoch(r_epoch){}
    };

private:
	paddedAtomic<uint64_t>* upper_reservs;
	paddedAtomic<uint64_t>* lower_reservs;
	padded<uint64_t>* retire_counters;
	padded<uint64_t>* alloc_counters;
	padded<std::list<IntervalInfo>>* retired; 

	std::atomic<uint64_t> epoch;

    function<bool(T** , T*, bool)> isSafe;
    PAD;

    /* to read birth epoch from object*/
	uint64_t read_birth(T* obj){
        return obj->birth_epoch;
    }
    inline uint64_t getEpoch() { 
		return epoch.load(std::memory_order_acquire);
    }

	bool conflict(uint64_t* lower_epochs, uint64_t* upper_epochs, uint64_t birth_epoch, uint64_t retire_epoch){
		for (int i = 0; i < task_num; i++){
			if (upper_epochs[i] >= birth_epoch && lower_epochs[i] <= retire_epoch){
				return true;
			}
		}
		return false;
	}

    void empty(const int tid){
		//read all epochs
		uint64_t upper_epochs_arr[task_num];
		uint64_t lower_epochs_arr[task_num];
		for (int i = 0; i < task_num; i++){
			//sequence matters.
			lower_epochs_arr[i] = lower_reservs[i].ui.load(std::memory_order_acquire);
			upper_epochs_arr[i] = upper_reservs[i].ui.load(std::memory_order_acquire);
		}

		// erase safe objects
		std::list<IntervalInfo>* myTrash = &(retired[tid].ui);
        
        uint before_sz = myTrash->size();
        TRACE COUTATOMICTID("decided to empty! bag size="<<myTrash->size()<<std::endl);
		
        for (auto iterator = myTrash->begin(), end = myTrash->end(); iterator != end; ) {
			IntervalInfo res = *iterator;
			if(!conflict(lower_epochs_arr, upper_epochs_arr, res.birth_epoch, res.retire_epoch)){
                this->pool->add(tid, res.obj);
                iterator = myTrash->erase(iterator);//return iterator corresponding to next of last erased item
    		}
			else{++iterator;}
		}

        uint after_sz = myTrash->size();
        TRACE COUTATOMICTID("After empty! bag size="<<after_sz<<" reclaimed="<<(before_sz-after_sz)<<std::endl<<std::endl);
    }

public:
    template<typename _Tp1>
    struct rebind {
        typedef reclaimer_ibr<_Tp1, Pool> other;
    };
    template<typename _Tp1, typename _Tp2>
    struct rebind2 {
        typedef reclaimer_ibr<_Tp1, _Tp2> other;
    };

    template <typename First, typename... Rest>
    inline bool startOp(const int tid, void * const * const reclaimers, const int numReclaimers, const bool readOnly = false) {
        bool result = true;

		uint64_t e = epoch.load(std::memory_order_acquire);
		lower_reservs[tid].ui.store(e,std::memory_order_seq_cst);
		upper_reservs[tid].ui.store(e,std::memory_order_seq_cst);

        //simulating when to inc epoch, ibr has epoc freq as function of num allocs. I shall have numops count instead of alloc count.
		alloc_counters[tid] = alloc_counters[tid]+1;
		if(alloc_counters[tid]%(epochFreq*task_num)==0){
			epoch.fetch_add(1,std::memory_order_acq_rel);
		}
        return result;
    }

    inline void endOp(const int tid) {
		upper_reservs[tid].ui.store(UINT64_MAX,std::memory_order_release);
		lower_reservs[tid].ui.store(UINT64_MAX,std::memory_order_release);
    }

    /**Inner utility method for protect*/
    void read(int tid, T* obj){
        uint64_t prev_epoch = upper_reservs[tid].ui.load(std::memory_order_acquire);
		while(true){
			uint64_t curr_epoch = getEpoch();
			if (curr_epoch == prev_epoch)
            {
				return; //fast path to avoid a store if epoch hasnt changed
			}
            else 
            {
				// upper_reservs[tid].ui.store(curr_epoch, std::memory_order_release);
				upper_reservs[tid].ui.store(curr_epoch, std::memory_order_seq_cst);
				prev_epoch = curr_epoch;
			}
		}
    }

    inline bool isSafeFunctorAvailable(){
        return (isSafe != nullptr);
    }

    //pass &ofptr you wanna project stored in the already protected object and marked flag of already protected object.
    inline T* protect_read(const int tid, uint64_t& index, T** ptrToObj, bool& ptrToObjMarked){
        assert(isSafeFunctorAvailable());
        T* obj = *ptrToObj;
        read(tid, obj); //reserves the current epoch.


        if (!isSafe(ptrToObj, obj, ptrToObjMarked)){
            TRACE COUTATOMICTID ("reading ptr="<<obj<<" failed"<<std::endl);
            return NULL; // TODO return special Node;
        }
        // index++;
        return obj;
    }
    inline void unprotect_read(const int tid, uint64_t& index){
    }

    // Hijacking HEProtect as IBR too needs per object read. May once all mental FOG about IBR based per object clears away I can decide on a generic name for all reclaimers requiring per read ops like HE HP and IBR.
    inline bool HEProtect(const int tid, T* obj, const int index, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true)
    {
        read(tid, obj); //reserves the current epoch. NOTE: index is unused.

        // NOTE: This barrier not needed as read() has an implicit fence/xchg instruction in its assebly.
        // if (memoryBarrier){
        //     // assert(0&&"checking if barrier active or not! remove assert post confirmation.");
        //     __sync_synchronize(); // prevent not retired validation from moving before I reserve the epoch. Like actual setbench approach. RETHINK.
        // } 

        //Validate if node I'v read is still reachable from it previous protected node and the prev node is not marked. 
        if (notRetiredCallback(callbackArg)) {
            TRACE std::cout<<"notRetiredCallback returns true"<<std::endl;
            return true;
        } else {
            TRACE std::cout<<"notRetiredCallback returns false"<<std::endl;
            return false;
        }        
    }
    inline void HEUnprotect(const int tid, const int index){
        //nothing to be done to unprotect for IBR
    }
    
    // for all schemes except reference counting
    inline void retire(const int tid, T* obj) {
		if(obj==NULL){return;}
        uint64_t birth_epoch = read_birth(obj);
		std::list<IntervalInfo>* myTrash = &(retired[tid].ui);
		// for(auto it = myTrash->begin(); it!=myTrash->end(); it++){
		// 	assert(it->obj!=obj && "double retire error");
		// }
			
		uint64_t retire_epoch = epoch.load(std::memory_order_acquire);
		myTrash->push_back(IntervalInfo(obj, birth_epoch, retire_epoch));
		retire_counters[tid]=retire_counters[tid]+1;
		if(retire_counters[tid]%freq==0){
			empty(tid);
		}
    }

    void debugPrintStatus(const int tid) {
    }

    //dummy declaration
    void initThread(const int tid) { }
    void deinitThread(const int tid) { }
    inline static bool isProtected(const int tid, T * const obj) { return true;}
    inline static bool isQProtected(const int tid, T * const obj) { return false; }
    inline static bool protect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void unprotect(const int tid, T * const obj) {}
        
    inline static bool qProtect(const int tid, T * const obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool memoryBarrier = true) { return true; }
    inline static void qUnprotectAll(const int tid) {}    
    inline static bool quiescenceIsPerRecordType() { return false;}



    /*
    * Tells whether the reclaimer requires the DS Node objects needs to provide some obj metadata to reclaimer like birthera of a node
    */
   //not needed for ibr_rcu needed for HE remove when creating HE. Here for testing of hacky birthera mechanism only.
    inline static bool needsDSObjMetaData(){
        return true;
    }

    inline static std::string getReclaimerName(){
        return "ibr";
    }

    //Enables use of Hazard protection version of DS operations.
    inline static bool supportsCrashRecovery() { return supportsPerObjectProtection(); //to avoid changing asserts in record mgr.
    }
    inline static bool supportsPerObjectProtection() { return true; }

#ifdef BIRTH_EPOCH_NEEDED
    //this is public API which shall call privateAPI. 
    inline uint64_t getBirthEpoch(){
        return getEpoch();
    }
#endif // BIRTH_EPOCH_NEEDED

    /**
     * To escape asserts at record manager assert(!supportcrash || isQuiescent())
    */
    inline static bool isQuiescent(const int tid) {
        return true;
    }    

    

    reclaimer_ibr(const int numProcesses, Pool *_pool, debugInfo * const _debug, RecoveryMgr<void *> * const _recoveryMgr = NULL)
            : reclaimer_interface<T, Pool>(numProcesses, _pool, _debug, _recoveryMgr) {
        VERBOSE std::cout<<"constructor reclaimer_ibr helping="<<this->shouldHelp()<<std::endl;
        task_num = numProcesses;
        freq = 30;
        epochFreq = 150;

		retired = new padded<std::list<IntervalInfo>>[task_num];
		upper_reservs = new paddedAtomic<uint64_t>[task_num];
		lower_reservs = new paddedAtomic<uint64_t>[task_num];
		for (int i = 0; i < task_num; i++){
			upper_reservs[i].ui.store(UINT64_MAX, std::memory_order_release);
			lower_reservs[i].ui.store(UINT64_MAX, std::memory_order_release);
		}
		retire_counters = new padded<uint64_t>[task_num];
		alloc_counters = new padded<uint64_t>[task_num];
		epoch.store(0,std::memory_order_release);
        isSafe = [](T** ptrToObj, T* obj, bool nodeContainingPtrToObjIsMarked){ if( (*ptrToObj) == obj){ if(!nodeContainingPtrToObjIsMarked){ TRACE COUTATOMICTID("in functor="<<obj<<std::endl); return true;} } return false; };
    }
    ~reclaimer_ibr() {
    }

};

#endif //RECLAIM_IBR_H


