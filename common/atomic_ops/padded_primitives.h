/**
 * The file contains wrapper for padding  concurrent primitives. Taken from IBR source code to integrate
 * it with setbench.
 * source: https://github.com/roghnin/Interval-Based-Reclamation, By Haosen Wen, Joseph Izraelevitz, Wentao Cai, H. Alan Beadle, Michael L. Scott
*/



#ifndef CONCURRENT_PRIMITIVES_HPP
#define CONCURRENT_PRIMITIVES_HPP

#include <assert.h>
#include <stddef.h>
#include <iostream>
#include <atomic>
#include <string>

#define CACHE_LINE_SIZE BYTES_IN_CACHE_LINE

// Possibly helpful concurrent data structure primitives
// Pads data to cacheline size to eliminate false sharing



template<typename T>
class padded {
public:
   //[[ align(CACHE_LINE_SIZE) ]] T ui;	
	T ui;
private:
   /*uint8_t pad[ CACHE_LINE_SIZE > sizeof(T)
        ? CACHE_LINE_SIZE - sizeof(T)
        : 1 ];*/
	uint8_t pad[ 0 != sizeof(T)%CACHE_LINE_SIZE
        ?  CACHE_LINE_SIZE - (sizeof(T)%CACHE_LINE_SIZE)
        : CACHE_LINE_SIZE ];
public:
  padded<T> ():ui() {};
  // conversion from T (constructor):
  padded<T> (const T& val):ui(val) {};
  // conversion from A (assignment):
  padded<T>& operator= (const T& val) {ui = val; return *this;}
  // conversion to A (type-cast operator)
  operator T() {return T(ui);}
};//__attribute__(( aligned(CACHE_LINE_SIZE) )); // alignment confuses valgrind by shifting bits


template<typename T>
class paddedAtomic {
public:
   //[[ align(CACHE_LINE_SIZE) ]] T ui;	
	std::atomic<T> ui;
private:
	uint8_t pad[ 0 != sizeof(T)%CACHE_LINE_SIZE
        ?  CACHE_LINE_SIZE - (sizeof(T)%CACHE_LINE_SIZE)
        : CACHE_LINE_SIZE ];
public:
  paddedAtomic<T> ():ui() {}
  // conversion from T (constructor):
  paddedAtomic<T> (const T& val):ui(val) {}
  // conversion from A (assignment):
  paddedAtomic<T>& operator= (const T& val) {ui.store(val); return *this;}
  // conversion to A (type-cast operator)
  operator T() {return T(ui.load());}
};//__attribute__(( aligned(CACHE_LINE_SIZE) )); // alignment confuses valgrind by shifting bits



template<typename T>
class volatile_padded {
public:
   //[[ align(CACHE_LINE_SIZE) ]] volatile T ui;	
	volatile T ui;
private:
   uint8_t pad[ CACHE_LINE_SIZE > sizeof(T)
        ? CACHE_LINE_SIZE - sizeof(T)
        : 1 ];
public:
  volatile_padded<T> ():ui() {}
  // conversion from T (constructor):
  volatile_padded<T> (const T& val):ui(val) {}
  // conversion from T (assignment):
  volatile_padded<T>& operator= (const T& val) {ui = val; return *this;}
  // conversion to T (type-cast operator)
  operator T() {return T(ui);}
}__attribute__(( aligned(CACHE_LINE_SIZE) ));

#endif
