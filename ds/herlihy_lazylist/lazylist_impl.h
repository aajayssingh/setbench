/**
 *   This algorithm is based on: 
 *   A Lazy Concurrent List-Based Set Algorithm,
 *   S. Heller, M. Herlihy, V. Luchangco, M. Moir, W.N. Scherer III, N. Shavit
 *   OPODIS 2005
 *   
 *   The implementation is based on implementations by:
 *   Vincent Gramoli https://sites.google.com/site/synchrobench/
 *   Vasileios Trigonakis http://lpd.epfl.ch/site/ascylib - http://lpd.epfl.ch/site/optik
 */

#ifndef LAZYLIST_IMPL_H
#define LAZYLIST_IMPL_H

#include <cassert>
#include <csignal>
#include "locks_impl.h"
#include "lazylist.h"


#ifndef casword_t
#define casword_t uintptr_t
#endif

template <typename K, typename V, class RecManager>
lazylist<K,V,RecManager>::lazylist(const int numProcesses, const K _KEY_MIN, const K _KEY_MAX, const V _NO_VALUE, unsigned int id)
        : recordmgr(new RecManager(numProcesses, /*SIGRTMIN+1*/SIGQUIT)), KEY_MIN(_KEY_MIN), KEY_MAX(_KEY_MAX), NO_VALUE(_NO_VALUE)
{
    const int tid = 0;
    initThread(tid);
    nodeptr max = new_node(tid, KEY_MAX, 0, NULL);
    head = new_node(tid, KEY_MIN, 0, max);

    //TODO:currently using this API in place of supportsPerObjectProtection() sincdebra+ is not in use.
    NOT_SAFE = recordmgr->template getNotSafeNode<node_t<K,V>>();
    const string recName = recordmgr->getReclaimerName();
    if(recName == "ibr" || recName == "he" || recName == "hp") {
        assert( recordmgr->isSafeFunctorAvailable() && "isSafe() shall be inited in reclaimers constructor" );
    }
}

template <typename K, typename V, class RecManager>
lazylist<K,V,RecManager>::~lazylist() {
    const int dummyTid = 0;
    nodeptr curr = head;
    while (curr->key < KEY_MAX) {
        nodeptr next = curr->next;
        recordmgr->deallocate(dummyTid, curr);
        curr = next;
    }
    recordmgr->deallocate(dummyTid, curr);
    
    recordmgr->printStatus();
    
    delete recordmgr;
    #ifdef USE_DEBUGCOUNTERS
    delete counters;
#endif
}

template <typename K, typename V, class RecManager>
void lazylist<K,V,RecManager>::initThread(const int tid) {
//    if (init[tid]) return; else init[tid] = !init[tid];

    recordmgr->initThread(tid);
}

template <typename K, typename V, class RecManager>
void lazylist<K,V,RecManager>::deinitThread(const int tid) {
//    if (!init[tid]) return; else init[tid] = !init[tid];

    recordmgr->deinitThread(tid);
}

template <typename K, typename V, class RecManager>
nodeptr lazylist<K,V,RecManager>::new_node(const int tid, const K& key, const V& val, nodeptr next) {
    nodeptr nnode = recordmgr->template allocate<node_t<K,V> >(tid);
    if (nnode == NULL) {
        cout<<"out of memory"<<endl;
        exit(1);
    }
    nnode->key = key;
    nnode->val = val;
    nnode->next = next;
    nnode->lock = false;
    nnode->marked = 0;
    //TODO: While experimenting not all algos need birth epoch(like debra, qsbr, ibr_rcu), therefore they will incur extra cost of populating and making a function call per
    // allocation. Solution is to do separate compilation with this flag. Only ibr and he which need birth eras shall compile with this flag.
    #ifdef BIRTH_EPOCH_NEEDED
    nnode->birth_epoch = recordmgr->getBirthEpoch();
    #endif // BIRTH_EPOCH_NEEDED

    return nnode;
}

template <typename K, typename V, class RecManager>
inline bool lazylist<K,V,RecManager>::validateLinks(const int tid, nodeptr pred, nodeptr curr) {
    return (!pred->marked && !curr->marked && pred->next == curr);
}

template <typename K, typename V, class RecManager>
bool lazylist<K,V,RecManager>::contains(const int tid, const K& key) {
//COUTATOMIC("tr or debra"<<recordmgr->supportsCrashRecovery()<<endl);
if(!recordmgr->supportsCrashRecovery()){
    while(recordmgr->needsSetJmp() && sigsetjmp(setjmpbuffers[tid], 1)) {}
    
    recordmgr->startOp(tid);
    nodeptr curr = head;
    while (curr->key < key) {
        curr = curr->next;
    }

    V res = NO_VALUE; 
    if ((curr->key == key) && !curr->marked) {
        res = curr->val;
    }
    recordmgr->endOp(tid);
    return (res != NO_VALUE);
}else{
    nodeptr curr;
    nodeptr pred;
    retry:
        recordmgr->startOp(tid);
        uint64_t index_counter = 0; //index in reservation array
        // pred = head;
        pred = recordmgr->protect_read(tid, index_counter, &head, (bool&)head->marked);
        if( pred == NOT_SAFE ) {
            recordmgr->endOp(tid); 
            goto retry;
        }

        // curr = pred->next;
        curr = recordmgr->protect_read (tid, index_counter, &pred->next, (bool&)pred->marked);
        if( curr == NOT_SAFE ){
            recordmgr->endOp(tid); 
            goto retry;
        }

        while (curr->key < key) {
            recordmgr->unprotect_read(tid, index_counter);
            pred = curr;
            // curr = pred->next;
            curr = recordmgr->protect_read(tid, index_counter, &pred->next, (bool&)pred->marked);
            if(curr == NOT_SAFE ){
                recordmgr->endOp(tid); 
                goto retry;
            }
        }

        V res = NO_VALUE; 
        if ((curr->key == key) && !curr->marked) {
            res = curr->val;
        }
        recordmgr->endOp(tid);
        return (res != NO_VALUE);
}
}

template <typename K, typename V, class RecManager>
V lazylist<K,V,RecManager>::doInsert(const int tid, const K& key, const V& val, bool onlyIfAbsent) {
if(!recordmgr->supportsCrashRecovery()){
    nodeptr curr;
    nodeptr pred;
    nodeptr newnode;
    V result;
    retry_nonibr:
        while(recordmgr->needsSetJmp() && sigsetjmp(setjmpbuffers[tid], 1)) {}    
        recordmgr->startOp(tid);  //checks if retiredbag full, neutralize all free ret bag.
        
        pred = head;
        curr = pred->next;
        while (curr->key < key) {
            pred = curr;
            curr = curr->next;
        }

        if(recordmgr->needsSetJmp()) recordmgr->saveForWritePhase(tid, pred);
        if(recordmgr->needsSetJmp()) recordmgr->upgradeToWritePhase(tid);
        acquireLock(&(pred->lock));
        if (validateLinks(tid, pred, curr)) {
            if (curr->key == key) { //key is in list
                V result = curr->val;
                releaseLock(&(pred->lock));
                recordmgr->endOp(tid);
                return result; //failed
            }
            // success: key not in list insert
            assert(curr->key != key);
            result = NO_VALUE;
            newnode = new_node(tid, key, val, curr);
            pred->next = newnode;

            releaseLock(&(pred->lock));
            recordmgr->endOp(tid);
            return result;
        }
        releaseLock(&(pred->lock));
        recordmgr->endOp(tid);
        goto retry_nonibr; //op failed due to contection retry. 
}else{
    ds_retired_info_t info;
    nodeptr curr;
    nodeptr pred;
    nodeptr newnode;
    V result;
    /**
     * Scheme for index counter: protect ---> ctr++ and cntr%2 ---> unprotect. This way, I can rotate in he reservation slots ensuring the index0,1 correspond to right nodes.
    */
    retry:
        recordmgr->startOp(tid);
        uint64_t index_counter = 0;

        // pred = head;
        pred = recordmgr->protect_read(tid, index_counter, &head, (bool&)head->marked);
        if( pred == NOT_SAFE ) {
            recordmgr->endOp(tid); 
            goto retry;
        }

        // curr = pred->next;
        curr = recordmgr->protect_read (tid, index_counter, &pred->next, (bool&)pred->marked);
        if( curr == NOT_SAFE ){
            recordmgr->endOp(tid); 
            goto retry;
        }

        while (curr->key < key) {
            recordmgr->unprotect_read(tid, index_counter);
            pred = curr;
            // curr = pred->next;
            curr = recordmgr->protect_read(tid, index_counter, &pred->next, (bool&)pred->marked);
            if( curr == NOT_SAFE ){
                recordmgr->endOp(tid); 
                goto retry;
            }
        }

        acquireLock(&(pred->lock));
        if (validateLinks(tid, pred, curr)) {
            if (curr->key == key) { //key is in list
                assert(curr->key == key);
                V result = curr->val;
                releaseLock(&(pred->lock));
                recordmgr->endOp(tid);
                return result; //failed
            }
            // success: key not in list insert
            assert(curr->key != key);
            result = NO_VALUE;
            newnode = new_node(tid, key, val, curr);
            pred->next = newnode;

            releaseLock(&(pred->lock));
            recordmgr->endOp(tid);
            return result;
        }
        releaseLock(&(pred->lock));
        recordmgr->endOp(tid);
        goto retry; //op failed due to contection retry.
}
}

/*
 * Logically remove an element by setting a mark bit to 1 
 * before removing it physically.
 */
template <typename K, typename V, class RecManager>
V lazylist<K,V,RecManager>::erase(const int tid, const K& key) {
if(!recordmgr->supportsCrashRecovery()){
    nodeptr pred;
    nodeptr curr;
    V result;
    retry_nonibr:
        while(recordmgr->needsSetJmp() && sigsetjmp(setjmpbuffers[tid], 1)) {}
        recordmgr->startOp(tid);

        pred = head;
        curr = pred->next;
        while (curr->key < key) {
            pred = curr;
            curr = curr->next;
        }

        if (curr->key != key) {
            result = NO_VALUE;
            recordmgr->endOp(tid); 
            return result;
        }

        if(recordmgr->needsSetJmp()){
            recordmgr->saveForWritePhase(tid, pred);
            recordmgr->saveForWritePhase(tid, curr);
            recordmgr->upgradeToWritePhase(tid);
        }
        
        acquireLock(&(pred->lock));
        acquireLock(&(curr->lock));
        if (validateLinks(tid, pred, curr)) {
            assert(curr->key == key);
            result = curr->val;
            nodeptr c_nxt = curr->next;

            curr->marked = 1;                                                   // LINEARIZATION POINT
            pred->next = c_nxt;

            recordmgr->retire(tid, curr);

            releaseLock(&(curr->lock));
            releaseLock(&(pred->lock));
            recordmgr->endOp(tid); 
            return result;
        }
        releaseLock(&(curr->lock));
        releaseLock(&(pred->lock));
        recordmgr->endOp(tid);
        goto retry_nonibr; //op failed due to contection retry. 
}else{
    ds_retired_info_t info;
    nodeptr pred;
    nodeptr curr;
    V result;
    retry:
        recordmgr->startOp(tid);
        uint64_t index_counter = 0;

        // pred = head;
        pred = recordmgr->protect_read(tid, index_counter, &head, (bool&)head->marked);
        if( pred == NOT_SAFE ) {
            recordmgr->endOp(tid); 
            goto retry;
        }

        // curr = pred->next;
        curr = recordmgr->protect_read (tid, index_counter, &pred->next, (bool&)pred->marked);
        if( curr == NOT_SAFE ){
            recordmgr->endOp(tid); 
            goto retry;
        }

        while (curr->key < key) {
            recordmgr->unprotect_read(tid, index_counter);
            pred = curr;
            // curr = pred->next;
            curr = recordmgr->protect_read(tid, index_counter, &pred->next, (bool&)pred->marked);
            if( curr == NOT_SAFE ){
                recordmgr->endOp(tid); 
                goto retry;
            }
        }

        if (curr->key != key) {
            result = NO_VALUE;
            recordmgr->endOp(tid); 
            return result;
        }
        __sync_synchronize();
        acquireLock(&(pred->lock));
        acquireLock(&(curr->lock));
        if (validateLinks(tid, pred, curr)) {
            
            if (curr->key != key) {
                COUTATOMICTID("curr->key="<<curr->key<<" key"<<key<<"curr->next="<<curr->next<<std::endl);
                assert(curr->key == key);
            }
            result = curr->val;
            nodeptr c_nxt = curr->next;

            curr->marked = 1;                                                   // LINEARIZATION POINT
            pred->next = c_nxt;

            recordmgr->retire(tid, curr);

            releaseLock(&(curr->lock));
            releaseLock(&(pred->lock));
            recordmgr->endOp(tid); 
            return result;
        }
        releaseLock(&(curr->lock));
        releaseLock(&(pred->lock));
        recordmgr->endOp(tid);
        goto retry; 
}
}

template <typename K, typename V, class RecManager>
long long lazylist<K,V,RecManager>::debugKeySum(nodeptr head) {
    long long result = 0;
    nodeptr curr = head->next;
    while (curr->key < KEY_MAX) {
        result += curr->key;
//        COUTATOMIC("-->"<<curr->key<<std::endl);
        curr = curr->next;
    }
    cout << "********************AJ checksum"<<result<<endl;
    return result;
}

template <typename K, typename V, class RecManager>
long long lazylist<K,V,RecManager>::getKeyChecksum() 
{
    return debugKeySum(head);
}

template <typename K, typename V, class RecManager>
long long lazylist<K,V,RecManager>::debugKeySum() {
//    COUTATOMIC("debugKeySum="<<debugKeySum(head)<<std::endl);
    return debugKeySum(head);
}

#endif	/* LAZYLIST_IMPL_H */
