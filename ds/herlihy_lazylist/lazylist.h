/* 
 * File:   lazylist.h
 * Author: trbot
 *
 * Created on February 17, 2016, 7:34 PM
 */

#ifndef LAZYLIST_H
#define	LAZYLIST_H
#include "record_manager.h"
#include "recovery_manager.h"

#include <iostream>
#include <string>
using namespace std;

template<typename K, typename V>
class node_t {
public:
    volatile K key;
    volatile V val;
    node_t<K,V> * next;
    volatile int lock;
    bool marked;
    #ifdef BIRTH_EPOCH_NEEDED
    uint64_t birth_epoch;
    #endif
};

#define nodeptr node_t<K,V> *

template <typename K, typename V, class RecManager>
class lazylist {
private:
    RecManager * const recordmgr;
    nodeptr head;
    nodeptr NOT_SAFE;


    const K KEY_MIN;
    const K KEY_MAX;
    const V NO_VALUE;

    bool validateLinks(const int tid, nodeptr pred, nodeptr curr);
    nodeptr new_node(const int tid, const K& key, const V& val, nodeptr next);
    long long debugKeySum(nodeptr head);

    V doInsert(const int tid, const K& key, const V& value, bool onlyIfAbsent);
    
    int init[MAX_THREADS_POW2] = {0,};

public:

    lazylist(int numProcesses, const K _KEY_MIN, const K _KEY_MAX, const V NO_VALUE, unsigned int id);
    ~lazylist();
    bool contains(const int tid, const K& key);
    V insertIfAbsent(const int tid, const K& key, const V& val) {
        return doInsert(tid, key, val, true);
    }
    V erase(const int tid, const K& key);
    
    void initThread(const int tid);
    void deinitThread(const int tid);
    
    long long debugKeySum();
    long long getKeyChecksum();
    bool validate(const long long keysum, const bool checkkeysum) {
        return true;
    }

    long long getSizeInNodes() {
        long long size = 0;
        for (nodeptr curr = head->next; curr->key != KEY_MAX; curr = curr->next) {
            ++size;
        }
        return size;
    }
    long long getSize() {
        long long size = 0;
        for (nodeptr curr = head->next; curr->key != KEY_MAX; curr = curr->next) {
            size += (!curr->marked);
        }
        return size;
    }
    string getSizeString() {
        stringstream ss;
        ss<<getSizeInNodes()<<" nodes in data structure";
        return ss.str();
    }
    
    RecManager * const debugGetRecMgr() {
        return recordmgr;
    }
   
    node_t<K,V> * debug_getEntryPoint() { return head; }
};

#include "lazylist_impl.h"
#endif	/* LAZYLIST_H */

