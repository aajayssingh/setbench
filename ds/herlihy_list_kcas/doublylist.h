/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   doublylist.h
 * Author: a529sing
 *
 * Created on March 4, 2020, 3:21 PM
 */

#pragma once
#define MAX_KCAS 41
#include <cassert>
#include "kcas.h"

#ifndef PADDING_BYTES
#define PADDING_BYTES 64
#endif

KCASLockFree<5> kcas;

template<typename K, typename V>
struct Node{
    K key;
    V val;
    casword_t marked;
    casword_t next;
    casword_t prev;
    Node(){}
    void init_noderec_t(const int tid, const int _key, Node* succ, Node* pred){
        key = _key;
        kcas.writeInitPtr(tid, &next, (casword_t)succ);
        kcas.writeInitPtr(tid, &prev, (casword_t)pred); //for head not needed to use kcas api
        kcas.writeInitPtr(tid, &marked, (casword_t)0);
    }
};

template<class RecordManager, typename K, typename V>
class DoublyLinkedListReclaim {
private:
    volatile char padding0[PADDING_BYTES];
    const int numThreads;
    const int minKey;
    const int maxKey;
    volatile char padding1[PADDING_BYTES];

   
    //AJ
    Node<K, V>*head;//for lr traversal
    volatile char padding2[PADDING_BYTES];
    Node<K, V> *tail; //with hkey maxkey+1
    volatile char padding3[PADDING_BYTES];
    //recmgr
    //RecordManager<Node> *recmgr; //// or pointer type Node?
    RecordManager * const recmgr;

public:
    DoublyLinkedListReclaim(const int _numThreads, const int _minKey, const int _maxKey);
    ~DoublyLinkedListReclaim();
    
    void initThread(const int tid);
    void deinitThread(const int tid);
    bool validate();
    
    pair<Node<K, V>*, Node<K, V>*> localSearch(const int tid, const int & key);
    
    bool contains(const int tid, const int & key);
    V insertIfAbsent(const int tid, const K &key, const V &value); // try to insert key; return true if successful (if it doesn't already exist), false otherwise
    V erase(const int tid, const int & key); // try to erase key; return true if successful, false otherwise
    
    long getSumOfKeys(); // should return the sum of all keys in the set
    void printDebuggingDetails(); // print any debugging details you want at the end of a trial in this function
};

template<class RecordManager, typename K, typename V>
DoublyLinkedListReclaim<RecordManager,  K,  V>::DoublyLinkedListReclaim(const int _numThreads, const int _minKey, const int _maxKey)
        : numThreads(_numThreads), minKey(_minKey), maxKey(_maxKey), recmgr(new RecordManager(numThreads)) {
    // it may be useful to know about / use the "placement new" operator (google)
    // because the simple_record_manager::allocate does not take constructor arguments
    // ... placement new essentially lets you call a constructor after an object already exists.
//    cout<<sizeof(Node)<<endl;
    int tid = 0;
//    recmgr = new RecordManager(numThreads);//new simple_record_manager<Node<K, V> >(numThreads);// or pointer type Node?
    
    recmgr->getGuard(tid);

    //init the list with sentinel nodes
    head = recmgr->template allocate<Node<K, V>>(tid);
    head->init_noderec_t(tid, minKey, (casword_t)NULL, (casword_t)NULL);
//    cout<<"head:"<<head<<" "<<head->key<<endl;
    
    tail = recmgr-> template allocate<Node<K, V> >(tid); //new (memory_tail) Node(maxKey+1); //placement new
    tail->init_noderec_t(tid, maxKey+1, (casword_t)NULL, (casword_t)NULL);
//    cout<<"tail:"<<tail<<" "<<tail->key<<endl;
    
    //connect head and tail
    kcas.writeInitPtr(tid, &head->next, (casword_t)tail);
    kcas.writeInitPtr(tid, &tail->prev, (casword_t)head);
}

template<class RecordManager, typename K, typename V>
DoublyLinkedListReclaim<RecordManager,  K,  V>::~DoublyLinkedListReclaim() {
    //AJ: free list with iteration.
    Node<K, V> *curr = head;
    int tid = 0;
    recmgr->getGuard(tid);
    
    while (curr->key <= maxKey) //free tail too
    {
        Node<K, V> *pred = curr;
        curr = (Node<K, V> *)kcas.readPtr(tid, &curr->next);
        //free pred
        recmgr->deallocate(tid, pred); //is this freeing mem. Prob not everything alloacted on heap I should delete.
        //delete pred;
    }

    recmgr->deallocate(tid, curr);
    delete recmgr;
    head = NULL;
    tail = NULL;
}

/*note recmgr guard already taken in ops called frm*/
template <class RecordManager, typename K, typename V>
pair<Node<K, V>*, Node<K, V>*> DoublyLinkedListReclaim<RecordManager,  K,  V>::localSearch(const int tid, const int & key){
    Node<K, V> *pred = head;
    Node<K, V> *succ = (Node<K, V> *)kcas.readPtr(tid, &head->next);
    
    while(true){
        if ( succ->key >= key)
            return make_pair(pred, succ);
        pred = succ;
        succ = (Node<K, V> *)kcas.readPtr(tid, &succ->next);
    }
}

template<class RecordManager, typename K, typename V>
bool DoublyLinkedListReclaim<RecordManager,  K,  V>::contains(const int tid, const int & key) {
    assert(key > minKey - 1 && key >= minKey && key <= maxKey && key < maxKey + 1);
    recmgr->getGuard(tid, true); //is it needed?
    pair<Node<K, V> *, Node<K, V> *> pred_succ_pair = localSearch(tid, key);
    return (pred_succ_pair.second->key == key);
}

template<class RecordManager, typename K, typename V>
V DoublyLinkedListReclaim<RecordManager,  K,  V>::insertIfAbsent(const int tid, const K &key, const V &value) {
    assert(key > minKey - 1 && key >= minKey && key <= maxKey && key < maxKey + 1);
    //keys range from 1 to max inclusive.
    while(true){
        recmgr->getGuard(tid);
        
        pair<Node<K, V> *, Node <K, V>*> pred_succ_pair = localSearch(tid, key);
        Node<K, V> * pred = pred_succ_pair.first;
        Node <K, V>* succ = pred_succ_pair.second;
        
        assert (pred != NULL && succ != NULL && " pred or succ is null");
        
        if (succ->key == key) return (V)key/*false*/;
        
        //create a new node
        Node<K, V> *n = recmgr->template allocate<Node<K, V>>(tid);
        n->init_noderec_t(tid, key, succ, pred);
        
        auto desc_ptr = kcas.getDescriptor(tid); //got a descp. now populate it.
        desc_ptr->addValAddr(&pred->marked, (casword_t)0, (casword_t)0);
        desc_ptr->addValAddr(&succ->marked, (casword_t)0, (casword_t)0);
        desc_ptr->addPtrAddr(&pred->next, (casword_t)succ, (casword_t)n);
        desc_ptr->addPtrAddr(&succ->prev, (casword_t)pred, (casword_t)n);
        if ( kcas.execute(tid, desc_ptr) ) return (V)key /*true*/;
        else {
            //TODO: delete n and retry
            recmgr->deallocate(tid, n); //nobody has access to this node except me. safe to free. Also there is no pointer held by this thread after this point.
        }        
    }//while(true)
}

template<class RecordManager, typename K, typename V>
V DoublyLinkedListReclaim<RecordManager,  K,  V>::erase(const int tid, const int & key) {
    assert(key > minKey - 1 && key >= minKey && key <= maxKey && key < maxKey + 1);
    while(true){
        recmgr->getGuard(tid);
        
        pair<Node<K, V> *, Node<K, V> *> pred_succ_pair = localSearch(tid, key);
        Node<K, V> * pred = pred_succ_pair.first;
        Node<K, V> * succ = pred_succ_pair.second;
        
        assert (pred != NULL && succ != NULL && " pred or succ is null");
        
        if (succ->key != key) return (V)key /*false*/;
        assert(succ->key != tail->key);
        //read after node
        Node<K, V> *after = (Node<K, V> *)kcas.readPtr(tid, &succ->next);
        
        //not setting deleted node's(succ) prev and next to null, other threads might be traversing over it.
        
        //prepare desc for erase kcas
        auto desc_ptr = kcas.getDescriptor(tid); //got a descp. now populate it.
        desc_ptr->addValAddr(&pred->marked, (casword_t)0, (casword_t)0);
        desc_ptr->addValAddr(&succ->marked, (casword_t)0, (casword_t)1);
        desc_ptr->addValAddr(&after->marked, (casword_t)0, (casword_t)0);
        desc_ptr->addPtrAddr(&pred->next, (casword_t)succ, (casword_t)after);
        desc_ptr->addPtrAddr(&after->prev, (casword_t)succ, (casword_t)pred);
        if ( kcas.execute(tid, desc_ptr) ){ 
            //succ deleted, now retire
            recmgr->retire(tid, succ);
            return (V)key /*true*/;
        }
    }//while(true)
}

template <class RecordManager, typename K, typename V>
bool DoublyLinkedListReclaim<RecordManager,  K,  V>::validate(){
    getSumOfKeys();
    return true;
}

template<class RecordManager, typename K, typename V>
long DoublyLinkedListReclaim<RecordManager,  K,  V>::getSumOfKeys() {
    long sum = 0;
    
    Node<K, V> *curr = (Node <K, V>*)kcas.readPtr(0, &head->next);
    
    while (curr->key <= maxKey)
    {
        sum += curr->key;
        curr = (Node<K, V> *)kcas.readPtr(0, &curr->next); 
    }
    
    return sum;
}

template<class RecordManager, typename K, typename V>
void DoublyLinkedListReclaim<RecordManager,  K,  V>::printDebuggingDetails() {
//    cout<<"DoublyLinkedListReclaim ..."<<endl;
//    Node *curr = (Node *)kcas.readPtr(0, &head->next);
//    cout<<"head->next:"<<curr<<endl;
//    cout<<head->key<<"-->";
//    while (curr->key <= 100 and curr->key <= maxKey)
//    {
//        cout<< curr->key<<"--> ";
//        curr = (Node *)kcas.readPtr(0, &curr->next); 
//    }
//    cout<<tail->key<<endl;
}



