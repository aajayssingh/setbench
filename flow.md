<span class="colour" style="color:rgb(0, 0, 0)">TODO</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> Arrange all IBR APIs together in each file like in record\_mgr.h etc..</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> write BIrthEra module for Gtree.</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> Write discussed protect call module with list first. Need hazardptr to be changed as well.</span>

<br>
<span class="colour" style="color:rgb(0, 0, 0)">Why safe interface is like that?</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- need to pass node containing ptrtoObjects 's next and marked field\. As some ds will have left and right\. So to be generic just pass full member fields address\.</span><span class="colour" style="color:rgb(128, 0, 0)">**\# Post integration cleanup thoughts**</span>

<span class="colour" style="color:rgb(0, 0, 0)">reclaimer:</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- SOme reclaimers are very similar just one function definition changes for eg rcu EBR and QSBR \-\-\-\> define two diff reclaimer\.h \-\-\-\> optimal way\.</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- Add a function getReclaimerName\(\) which return current reclaimer running\. This will help to identify reclaimer and then call that reclaimers functionality at record manager\,</span>
<span class="colour" style="color:rgb(0, 0, 0)">for eg- If I need to call two different version of allocate() in allocator\_new.h based on type of reclaimer I can do that selection from record\_manger.h.</span>
<span class="colour" style="color:rgb(0, 0, 0)">allocator:</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- needs two diff versions of allocate\(\)\. 1\. default setbenches currently in use\. 2\. for the cases where allocate needs to tag anothe rinformation like birth epoch which each allocated node\.</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- probably freeing of these two differently allocatd nodes could alsop be done here in corresponding deallocate functions\. \-\-\-\> Confused about role of pool here\.</span>

<span class="colour" style="color:rgb(0, 0, 0)">data structure: This module seems to be more tricky to generalise:</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- Some reclaimers needs DS to provide to use certain function some do not \-\-\-\> eg: startOP\, retire\, reclaim\, endOP are common\. But\,</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- TR needs saveforwrite\(\)\, upgradetowrite\(\)\, Setjmp\(\) before startOP\(\) \-\-\> could be done using a Global START\_OP macro that setjmps and startOP\(\)</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- DEBRA\+ needs setjmp and an alternatepath upon sigjmp \-\-\>in Global START\_OP macro do setjmp and give alternate path \-\-\> do this with a if/else case based on if reclaimer name is DEBRA\+\.</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- Some reclaimers need a per object read\(\)::</span>
<span class="colour" style="color:rgb(0, 0, 0)">1 One version of read() needs only the next ptr to be returned as is. As if I was doing just a simple pointer dereference.</span>
<span class="colour" style="color:rgb(0, 0, 0)">2 One version of read() needs me to update reclaimer metadata(HP: save pointer, HE save epoch), dereference pointer to be read and validate if it hasnt been changed, HE read also needs index to save HE.</span>
<span class="colour" style="color:rgb(0, 0, 0)">Requirements are that We shall put less load on ds writer he should not be exposed to how he/she gets its pointer it wanna read next. Just a simple API shall do that. This implies that my read function would look more likeliterature SMRs that returns T\*.</span>
<span class="colour" style="color:rgb(0, 0, 0)">3 Another version of read() needs me to update reclaimer metadata (TagIBR: save upperEpoch), no validation about its existence needed.</span>
<span class="colour" style="color:rgb(0, 0, 0)">I donot want to implement three diff variants of DS for requirement 1 2 3 infact I can do that with one T\* read() function. Read based on type of reclaimer (1,2,3) implements its own version in its class.</span>

<span class="colour" style="color:rgb(0, 0, 0)">\- Some reclaimers need their DS nodes to be defined in a specific way fr example: TAGIBR requires DS nodes's next ptr to be also save some metadata and thus shall be atomic\. While for other reclaimers DS nodes could be just normal pointer\.</span>

<br>
<span class="colour" style="color:rgb(4, 81, 165)">\*</span><span class="colour" style="color:rgb(0, 0, 0)"> ummm QSBR is better than normal IBR.</span>

<span class="colour" style="color:rgb(0, 0, 0)">TODO Before meeting:</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> </span><span class="colour" style="color:rgb(0, 0, 0)">[</span><span class="colour" style="color:rgb(163, 21, 21)">check</span><span class="colour" style="color:rgb(0, 0, 0)">]</span><span class="colour" style="color:rgb(0, 0, 0)"> QSBR</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> </span><span class="colour" style="color:rgb(0, 0, 0)">[</span><span class="colour" style="color:rgb(163, 21, 21)">check</span><span class="colour" style="color:rgb(0, 0, 0)">]</span><span class="colour" style="color:rgb(0, 0, 0)"> getreclaimerName()</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> </span><span class="colour" style="color:rgb(0, 0, 0)">[</span><span class="colour" style="color:rgb(163, 21, 21)">check</span><span class="colour" style="color:rgb(0, 0, 0)">]</span><span class="colour" style="color:rgb(0, 0, 0)"> Normal EBR</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> </span><span class="colour" style="color:rgb(0, 0, 0)">[</span><span class="colour" style="color:rgb(163, 21, 21)">check</span><span class="colour" style="color:rgb(0, 0, 0)">]</span><span class="colour" style="color:rgb(0, 0, 0)"> Tag IBR (2GIBR most effecient version as per authors)</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> </span><span class="colour" style="color:rgb(0, 0, 0)">[</span><span class="colour" style="color:rgb(163, 21, 21)">check</span><span class="colour" style="color:rgb(0, 0, 0)">]</span><span class="colour" style="color:rgb(0, 0, 0)"> HE for list --> protection broken-- needs to enable macro so list results cannt be shown.</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> </span><span class="colour" style="color:rgb(0, 0, 0)">[</span><span class="colour" style="color:rgb(163, 21, 21)">check</span><span class="colour" style="color:rgb(0, 0, 0)">]</span><span class="colour" style="color:rgb(0, 0, 0)"> All above for list and Gtree</span>
<span class="colour" style="color:rgb(0, 0, 0)">\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-</span>

<span class="colour" style="color:rgb(0, 0, 0)">TODO failed to make it before time.</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> Michael list</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> Make Hazard pointer too working. --> If I wanna comapre all reclaimers, they shall execute without hiccups.</span>

<br>
<span class="colour" style="color:rgb(0, 0, 0)">TODO</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> Arrange all IBR APIs together in each file like in record\_mgr.h etc..</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> write BIrthEra module for Gtree.</span>
<span class="colour" style="color:rgb(4, 81, 165)">></span><span class="colour" style="color:rgb(0, 0, 0)"> Write discussed protect call module with list first. Need hazardptr to be changed as well.</span>

<br>
<span class="colour" style="color:rgb(0, 0, 0)">Why safe interface is like that ie passes T\*\* and marked param?</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- need to pass node containing ptrtoObjects 's next and marked field\. As some ds will have left and right\. So to be generic just pass full member fields address\.</span>
<span class="colour" style="color:rgb(0, 0, 0)">\- changing announce\[\] in hazardptr to IBR based atomic array to use index\.</span>